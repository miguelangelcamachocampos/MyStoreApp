//
//  VCAddItem.swift
//  MyStoreApp
//
//  Created by Mike on 3/28/19.
//  Copyright © 2019 Mike. All rights reserved.
//

import UIKit
import CoreData

class VCAddItem:
UIViewController, UIPickerViewDataSource, UIPickerViewDelegate ,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var txtItemName: UITextField!
    @IBOutlet weak var StoresPickView: UIPickerView!
    @IBOutlet weak var ivShowImage: UIImageView!
    
    
    
    var imagePicker:UIImagePickerController!
    var listOfStores = [StoreType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadStores()
        StoresPickView.delegate = self
        StoresPickView.dataSource = self
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func loadStores(){
        let fetchrequest: NSFetchRequest<StoreType> = StoreType.fetchRequest()
        do{
            listOfStores = try context.fetch(fetchrequest)
        }catch{
            print("cannot load stores")
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listOfStores.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let store = listOfStores[row]
        return store.store_name
      
    }
    
    @IBAction func buSelectPicture(_ sender: Any) {
        //TODO: Select imagre from phone
        present(imagePicker, animated: true, completion: nil)
        print("select image")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)  {
            ivShowImage.image = image
            
        }
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func buSave(_ sender: Any) {
        
        let newItem = Items(context: context)
        newItem.item_name = txtItemName.text
        newItem.date_add = NSDate() as? Date
        newItem.image = ivShowImage.image
        newItem.toStoreType = listOfStores[StoresPickView.selectedRow(inComponent: 0)]
        do{
            try ad.saveContext()
            txtItemName.text = ""
        }catch {
            print("Error cannot save item")
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
