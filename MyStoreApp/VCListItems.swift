//
//  VCListItems.swift
//  MyStoreApp
//
//  Created by Mike on 3/29/19.
//  Copyright © 2019 Mike. All rights reserved.
//

import UIKit
import CoreData


class VCListItems: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableViewItems: UITableView!
    
    
    var listOfItems = [Items]()
    override func viewDidLoad() { 
        super.viewDidLoad()
        loadItems()
        tableViewItems.delegate = self
        tableViewItems.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func loadItems(){
        let fetchrequest: NSFetchRequest<Items> = Items.fetchRequest()
        do{
            listOfItems = try context.fetch(fetchrequest)
        }catch{
            print("cannot load stores")
        }
    }
    
    //tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! TCVItem
        cell.setItem(item: listOfItems[indexPath.row])
        return cell
    }

}
